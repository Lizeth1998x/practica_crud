db.Users.drop();
db.Houses.drop();

let users = [
    { name: "Lizeth", lastName: "Ramos", age: 23, pets: [{ name: 'fulano', type: 'dog' }, { name: 'misifus', type: 'cat' }] },
    { name: "Javier", lastName: "Garcia", age: 21 },
    { name: "Daniel", lastName: "Lozano", age: 22 },
    { name: "Alejandro", lastName: "Hernandez", age: 24 },
    { name: "Roberto", lastName: "Perez", age: 20 },
    { name: "Dario", lastName: "Guzman", age: 19 },
];

db.Users.insert(users);

// for (let i = 0; i < 10; i++) {
//     db.Users.insert({name: "pedro", matricula:(100+i)});

// }

let houses = [
    { color: 'red', size: '8m', address: { city: 'Leon', country: 'Mexico' } },
];

db.Houses.insert(houses);

db.Houses.aggregate(
    {
        $lookup: {
            from: "Users",
            localField: 'user',
            foreignField: '_id',
            as: 'user',
        },
    },
    { $match: {color: "red"}},
    {$project: {color:1, address: 1, userChido: '$user'}}
).pretty();