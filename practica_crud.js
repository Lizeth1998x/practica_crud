//Create
db.Students.drop();
db.Groups.drop();

let students = [
    { name: "Lizeth", lastName: "Ramos", major: 'ITSN', extras: [{ name: 'Volley', type: 'sports' }, { name: 'polinesias', type: 'culture' }] },
    { name: "Javier", lastName: "Garcia", major: 'ISSC' },
    { name: "Daniel", lastName: "Lozano", major: 'IQ' },
    { name: "Alejandro", lastName: "Hernandez", major: 'BM' },
    { name: "Roberto", lastName: "Perez", major: 'ISSC' },
    { name: "Dario", lastName: "Guzman", major: 'ITSN' },
];

db.Students.insert(students);

let groups = [
    { grade: 1, sujects: { tech: 'Programming', teacher: 'Enrique' } },
];

db.Groups.insert(groups);


//read
db.Students.find({}).pretty()
db.Students.find({ "extras.name": "Volley" })

//update
db.Students.update({ "_id": ObjectId("61ac472ed23e5cff60d58be2")}, {$push:{extras:{"name": "football", "type":"sports"}}})


//delete
db.Students.update({'_id': ObjectId("61ac472ed23e5cff60d58be1")},{$pull: {extras:{type: 'culture'}}})